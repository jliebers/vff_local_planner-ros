# vff_local_planner

Implementation of the Virtual Force Field (VFF), published by J. Borenstein and Y. Koren in 1989 [0] as a local planner
plugin to be used within the navigation stack of ROS [1].

[0] https://doi.org/10.1109/21.44033

[1] https://ros.org
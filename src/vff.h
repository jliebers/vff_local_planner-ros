#ifndef VFF_LOCAL_PLANNER_VFF_H
#define VFF_LOCAL_PLANNER_VFF_H

#include "terminal_color_definitions.h"
#include <cmath>
#include "vector.h"
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Path.h>
#include <nav_core/base_local_planner.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>
#include <pluginlib/class_list_macros.h>


/**
  Vector Field Force Local Planning Algorithm, using a potential field.
 */

namespace VFF_local_planner
{

class VFF : public nav_core::BaseLocalPlanner
{
public:
  /* Implementing nav_core::BaseGlobalPlanne Interface
   * http://docs.ros.org/api/nav_core/html/classnav__core_1_1BaseLocalPlanner.html */
  void initialize(std::string name, tf::TransformListener* tf, costmap_2d::Costmap2DROS* costmap_ros);
  bool isGoalReached();
  bool setPlan(const std::vector<geometry_msgs::PoseStamped>& plan_);
  bool computeVelocityCommands(geometry_msgs::Twist& cmd_vel);

private:
  bool initialized_ = false;

  // Position and orientation [0; pi] of robot and goal in cartesian coordinate system.
  double pos_x_, pos_y_, pos_o_;
  double goal_x_, goal_y_, goal_o_;

  // Speed and safe orientation [0; pi] the robot should follow towards the goal:
  double speed_;
  double safe_o_;

  // Flags to indicate, whether robot has a) reached goal and b) is facing in right direction:
  bool reached_goal_, reached_dir_;

  // Settings obtained from parameter server (see vff_local_planner.yaml for semantics):
  double p_maxspeed_,               // robot's max speed [0; 1]
         p_fconst_,                 // attractive force constant
         p_potentialfield_radius_,  // radius of the potential field in cm.
         p_danger_radius_,          // radius of the "danger-zone", when robot is near obstacle
         p_danger_factor_,          // if robot is in danger, increase repulsive forces by factor
         p_distance2goal_,          // distance, where the robot considers the goal as met
         p_orientation2goal_,       // angle, at which the robot considers the goal as met
         p_goalradius_,             // picks the next goal as current target from the global plan path
         p_pid_kp_,                 // PID tuning variables
         p_pid_ki_,
         p_pid_kd_;
  int p_costmap_threshold_;         // which readings in the costmap are considered (254 = lethal)
  bool p_print_debugmap_, p_print_debugstate_; // whether debug information should be printed by ROS_INFO()

  // Internal variables for PID Controller:
  double pid_output__, pid_error__, pid_integral_error__, pid_last_error__, pid_derivative__, pid_sleep__;

  std::vector<geometry_msgs::PoseStamped> plan_; // obtained from global planner
  int plan_offset_;                              // index to current goal in plan
  std::string name_;

  costmap_2d::Costmap2DROS* cmap_ros_;  // Pointer to the costmap ros wrapper, received from the navigation stack
  costmap_2d::Costmap2D* cmap_;         // Pointer to the actual 2d costmap (obtained from the costmap ros wrapper)
  tf::TransformListener* tf_;

  void printDebugMap();
  void printDebugState(Vector repulsive_force, Vector attractive_force, Vector resulting_force);
  double pidController(double reference, double current_value);
  double getDistanceToPoint(double x, double y);
};

}
#endif  // VFF_LOCAL_PLANNER_VFF_H

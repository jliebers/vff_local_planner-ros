#include "vff.h"

using namespace VFF_local_planner;

void VFF::initialize(std::string name,
                     tf::TransformListener* tf,
                     costmap_2d::Costmap2DROS* costmap_ros)
{ 
  // process function arguments:
  this->cmap_ros_ = costmap_ros;
  this->cmap_ = costmap_ros->getCostmap();
  this->name_ = name;
  this->tf_ = tf;

  // initial position variables:
  this->goal_x_ = this->goal_y_ = this->safe_o_ = this->pos_x_ = this->pos_y_ = this->pos_o_ = 0;
  reached_goal_ = reached_dir_ = false;

  // initialize pid:
  pid_output__ = pid_error__ = pid_integral_error__ = pid_last_error__ = pid_derivative__ = 0;
  pid_sleep__ = 0.2;

  // Retrieve parameters from parameter server:
  if(!ros::param::get("vff_maxspeed", this->p_maxspeed_))
  {
    ROS_ERROR("Parameter \"/vff_maxspeed\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_fconst", this->p_fconst_))
  {
    ROS_ERROR("Parameter \"/vff_fconst\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_potentialfield_radius", this->p_potentialfield_radius_))
  {
    ROS_ERROR("Parameter \"/vff_potentialfield_radius\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_danger_radius", this->p_danger_radius_))
  {
    ROS_ERROR("Parameter \"/vff_danger_radius\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_danger_factor", this->p_danger_factor_))
  {
    ROS_ERROR("Parameter \"/vff_danger_factor\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_distance2goal", this->p_distance2goal_))
  {
    ROS_ERROR("Parameter \"/vff_distance2goal\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_orientation2goal", this->p_orientation2goal_))
  {
    ROS_ERROR("Parameter \"/vff_orientation2goal\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_goalradius", this->p_goalradius_))
  {
    ROS_ERROR("Parameter \"/vff_goalradius\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_costmap_threshold", this->p_costmap_threshold_))
  {
    ROS_ERROR("Parameter \"/vff_costmap_threshold\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_print_debugmap", this->p_print_debugmap_))
  {
    ROS_ERROR("Parameter \"/vff_print_debugmap\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_print_debugstate", this->p_print_debugstate_))
  {
    ROS_ERROR("Parameter \"/vff_print_debugstate\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_pid_kp", this->p_pid_kp_))
  {
    ROS_ERROR("Parameter \"/vff_pid_kp\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_pid_ki", this->p_pid_ki_))
  {
    ROS_ERROR("Parameter \"/vff_pid_ki\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  if(!ros::param::get("vff_pid_kd", this->p_pid_kd_))
  {
    ROS_ERROR("Parameter \"/vff_pid_kd\" was not found or had wrong type. "
              "Please check the VFF config file. Initialization failed.");
    return;
  }

  // Finish:
  ROS_INFO("Vector Field Force (VFF) Local Planner Plugin has been successfully initialized.");
  this->initialized_ = true;
}

bool VFF::computeVelocityCommands(geometry_msgs::Twist& cmd_vel)
{
  if (!initialized_)
  {
    ROS_ERROR("NES::VFF Local Planner Plugin has not been initialized. Please call initialize() first!\n");
    return false;
  }
  if(plan_.size() == 0)
  {
    ROS_ERROR("No plan given to local planner!");
    return false;
  }


  /*
   *    INITIALIZATION, SETUP AND IDENTIFICATION OF CURRENT GOAL
   */

  // Initialize cmd_vel:
  cmd_vel.linear.x = cmd_vel.linear.y = cmd_vel.linear.z = 0;
  cmd_vel.angular.x = cmd_vel.angular.y = cmd_vel.angular.z = 0;
  speed_ = p_maxspeed_; // will be reduced in case of obstacles


  // Get current position:
  tf::Stamped<tf::Pose> global_pose = tf::Stamped<tf::Pose>();
  this->cmap_ros_->getRobotPose(global_pose);
  pos_x_ = global_pose.getOrigin().x();
  pos_y_ = global_pose.getOrigin().y();
  pos_o_ = tf::getYaw(global_pose.getRotation());


  // get current goal from path, i.e. set offset to corresponding index in path:
  for(plan_offset_ = 0;
      plan_offset_ < plan_.size()
      && getDistanceToPoint(plan_[plan_offset_].pose.position.x, plan_[plan_offset_].pose.position.y) < p_goalradius_;
      plan_offset_++);

  ROS_INFO("Plan offset is: %i of %lu", plan_offset_, plan_.size());


  // set current goal:
  goal_x_ = plan_[plan_offset_].pose.position.x;
  goal_y_ = plan_[plan_offset_].pose.position.y;
  goal_o_ = tf::getYaw(plan_[plan_offset_].pose.orientation);



  /*
   *    TRAVERSAL OF COSTMAP AND CALCULATION OF FORCES
   */
  double middle_x = cmap_->getSizeInCellsX() / 2;
  double middle_y = cmap_->getSizeInCellsY() / 2;

  std::stack<Vector> readings_stack;  // stack of readings from costmap
  bool in_danger = false; // signalizes, if robot is dangerously near an obstacle

  // traverse costmap and form stack of readings from costmap:
  for (int x = 0; x < cmap_->getSizeInCellsX(); x++)
  {
    for (int y = 0; y < cmap_->getSizeInCellsY(); y++)
    {
      if (cmap_->getCost(y, x) >= p_costmap_threshold_)
      {
        // Create force vector for every reading in costmap:
        Vector vec = Vector(y - middle_y, x - middle_x);
        double vlen = std::sqrt(std::pow((x - middle_x),2)
                              + std::pow((y - middle_y),2));

        if (vlen > p_potentialfield_radius_)
          continue;  // skip too long reading

        if (vlen < p_danger_radius_)
          in_danger = true;

        vec /= std::pow(vlen, 2);

        readings_stack.push(vec * -1);  // all force vectors are pushed to the stack
      }
    }
  }

  // Calculate forces in potential field, IF the stack of readings is not empty
  Vector resulting_force = Vector(); // sum of all attractive and repulsive forces
  Vector attractive_force = Vector();
  Vector repulsive_force  = Vector();

  // If obstacles are in sight, sum them all up into "repulsive_force":
  while (!readings_stack.empty())
    {
      repulsive_force += readings_stack.top();
      readings_stack.pop();
    }

  // if dangerradius feature is used, maximize repulsive forces for immediate reaction of robot to nearby obstacle
  if (p_danger_radius_ && in_danger)
    repulsive_force *= p_danger_factor_;

  // damp speed according to repusive_force:
  speed_ = p_maxspeed_ / std::pow(repulsive_force.getLen(), 2);

  // form attractive_force:
  attractive_force = Vector(goal_x_ - pos_x_, goal_y_ - pos_y_).getUnitVector();
  attractive_force *= p_fconst_;

  resulting_force = attractive_force + repulsive_force;
  safe_o_ = atan2(resulting_force.getY(), resulting_force.getX());



  /*
   *    PID CONTROLLER, SPEED CALCULATION AND EXCEPTIONS
   */

  double pid_output = pidController(this->safe_o_, this->pos_o_);


  // robot is dangerously near an obstacle, but has already rotated, facing away? Make it go quickly.
  if (p_danger_radius_ && in_danger && std::abs(this->safe_o_ - this->pos_o_) < p_orientation2goal_)
    speed_ = p_maxspeed_;

  // Sanity check:
  if (speed_ > p_maxspeed_)
    speed_ = p_maxspeed_;


  if (reached_goal_) // robot has already reached the goal, needs to rotate in right direction.
    speed_ = 0;



  /*
   *    DEBUG PRINTOUTS
   */

  if(p_print_debugstate_)
    printDebugState(repulsive_force, attractive_force, resulting_force);
  if(p_print_debugmap_)
    printDebugMap();



  /*
   *    FORMING THE FINAL MESSAGE
   */

  cmd_vel.angular.z = pid_output;
  cmd_vel.linear.x = speed_;
  return true;
}

bool VFF::isGoalReached()
{
  reached_goal_ = getDistanceToPoint(goal_x_, goal_y_) < p_distance2goal_;
  reached_dir_ = (std::abs(goal_o_ - pos_o_) < p_orientation2goal_);

  return reached_goal_ && reached_dir_;
}

bool VFF::setPlan(const std::vector<geometry_msgs::PoseStamped>& plan)
{
  if (!initialized_)
  {
    ROS_ERROR("setPlan() was called before initialize() - please initialize() VFF local planner first!");
    return false;
  }
  this->plan_ = plan;
  reached_goal_ = reached_dir_ = false;

  // temporarily set final goal as current goal. Correct path offset will be identified in computeVelocityCommands():
  goal_x_ = plan_.back().pose.position.x;
  goal_y_ = plan_.back().pose.position.y;
  goal_o_ = tf::getYaw(plan_.back().pose.orientation);
  plan_offset_ = 0;

  ROS_INFO("New path consisting of %lu goals set. Final goal is at: (x|y|o) -> (%f|%f|%f)",
           plan_.size(), goal_x_, goal_y_, goal_o_);

  return true;
}

double VFF::pidController(double reference, double current_value)
{
  pid_error__ = reference - current_value;
  pid_integral_error__ += pid_error__ * pid_sleep__;
  pid_derivative__ = (pid_error__ - pid_last_error__) / pid_sleep__;
  pid_output__ = p_pid_kp_ * pid_error__ + p_pid_ki_ * pid_integral_error__ + p_pid_kd_ * pid_derivative__;
  pid_last_error__ = pid_error__;

  return pid_output__;
}

void VFF::printDebugState(Vector repulsive_force, Vector attractive_force, Vector resulting_force)
{
  ROS_INFO_STREAM("---------------------------------------------------------------------------");
  ROS_INFO("Position of current goal from path:      (x|y|o) -> (%f|%f|%f)", goal_x_, goal_y_, goal_o_);
  ROS_INFO("Position of robot received from costmap: (x|y|o) -> (%f|%f|%f)", pos_x_, pos_y_, pos_o_);
  ROS_INFO("Robot speed and orientation: %f @ %f", speed_, safe_o_);

  ROS_INFO_STREAM("[REP FORCE] \t" << std::to_string(repulsive_force.getX()) << "|"
              << std::to_string(repulsive_force.getY())
              << " \t " << std::to_string(repulsive_force.getLen())
  );


  ROS_INFO_STREAM("[ATT FORCE] \t" << std::to_string(attractive_force.getX()) << "|"
            << std::to_string(attractive_force.getY())
            << " \t " << std::to_string(attractive_force.getLen())
  );


  ROS_INFO_STREAM("[RES FORCE] \t" << std::to_string(resulting_force.getX()) << "|"
            << std::to_string(resulting_force.getY())
            << " \t " << std::to_string(resulting_force.getLen())
  );


  //ROS_INFO("Safe orientation is: \t %f ", result);
  //ROS_INFO("PID Output is: \t\t %f", pid_output);
  //ROS_INFO("Speed set to: \t\t %f", speed);

  ROS_INFO_STREAM("[GOAL]    "
    << "goal_x: " << std::to_string(goal_x_) << "\t"
    << "goal_y: " << std::to_string(goal_y_) << "\t"
    << "goal_o: " << std::to_string(goal_o_));

  ROS_INFO_STREAM("[ROBOT]   "
    << "pos_x:  " << std::to_string(pos_x_) << "\t"
    << "pos_y:  " << std::to_string(pos_y_) << "\t"
    << "pos_o:  " << std::to_string(pos_o_));
  ROS_INFO_STREAM("---------------------------------------------------------------------------");
}

void VFF::printDebugMap()
{
  double middle_x = cmap_->getSizeInCellsX() / 2;
  double middle_y = cmap_->getSizeInCellsY() / 2;
  // DEBUG PRINT MAP INFO
  ROS_INFO("Costmap has resolution of %u x %u Cells, which is %f x %f meters.", cmap_->getSizeInCellsX(),
           cmap_->getSizeInCellsY(), cmap_->getSizeInMetersX(), cmap_->getSizeInMetersY());

  // DEBUG PRINT MAP -- Print one char per iteration
  std::string map_print = "Costmap2D Debug Printout: \n";
  for(int y = cmap_->getSizeInCellsY(); y >= 0; y--)
    {
    for(int x = 0; x < cmap_->getSizeInCellsX(); x++)
      {
        if (x == 0 && y == 0)
        {  // Print 0|0
          map_print += "0";
        }
        else if (x == 0 && y == cmap_->getSizeInCellsY() - 1)
        {
          map_print += "+Y";
        }
        else if (y == 0 && x == cmap_->getSizeInCellsX() - 1)
        {
          map_print += "+X";
        }
        else if (x == cmap_->getSizeInCellsX() - 1 && y == cmap_->getSizeInCellsY() - 1)
        {
          map_print += "*";
        }
        else if (x == cmap_->getSizeInCellsX() / 2 && y == cmap_->getSizeInCellsY() / 2)
        {
          map_print += "M";
        }
        else if (cmap_->getCost(x, y) >= p_costmap_threshold_)
        {
          double vlen = std::sqrt(std::pow((x - middle_x),2)
                                + std::pow((y - middle_y),2));

          if (vlen > p_potentialfield_radius_)
          {  // too long, ignore
            map_print += "# ";
          }
          else if (vlen < p_danger_radius_)
          {  // dangerous distance
            map_print += RED;
            map_print += "#";
            map_print += RESET;
            map_print += " ";
          }
          else
          {  // considered valid reading
            map_print += YELLOW;
            map_print += "#";
            map_print += RESET;
            map_print += " ";
          }
          // ROS_INFO("Attention - Vector from pt (%f|%f) with len of %f detected!", y-m_y, x-m_x, vlen);
      }
      else
      {
        map_print += "  ";
      }
    }
    map_print += "\n";
  }
  map_print += "\n\n\n"; // end of loop
  ROS_INFO_STREAM(map_print);
}

// ---------------- UTILITY FUNCTIONS ------------------------

double VFF::getDistanceToPoint(double x, double y)
{
  return Vector(x - this->pos_x_, y - this->pos_y_).getLen();
}

PLUGINLIB_EXPORT_CLASS(VFF_local_planner::VFF, nav_core::BaseLocalPlanner)

#include "vector.h"

using namespace VFF_local_planner;

Vector::Vector()
{
  this->x_ = this->y_ = 0;
}

Vector::Vector(double x, double y)
{
  this->x_ = x;
  this->y_ = y;
}

void Vector::setX(double x)
{
  this->x_ = x;
}

void Vector::setY(double y){
  this->y_ = y;
}

double Vector::getX(){
  return this->x_;
}

double Vector::getY(){
  return this->y_;
}

double Vector::getLen(){
  return std::sqrt(std::pow(this->x_, 2) + std::pow(this->y_, 2));
}

Vector Vector::getUnitVector(){
  return Vector(this->x_, this->y_) / this->getLen();
}

Vector& Vector::operator+=(Vector v)
{
  this->x_ += v.x_;
  this->y_ += v.y_;

  return *this;
}

Vector& Vector::operator-=(Vector v)
{
  this->x_ -= v.x_;
  this->y_ -= v.y_;

  return *this;
}

Vector& Vector::operator*=(double d)
{
  this->x_ *= d;
  this->y_ *= d;

  return *this;
}

Vector& Vector::operator/=(double d)
{
  if (d == 0)
  {  // do not divide by zero
    ROS_ERROR("Vector has been divided by zero.");
    return *this;
  }

  this->x_ /= d;
  this->y_ /= d;

  return *this;
}

Vector Vector::operator+(Vector v)
{
  return Vector(this->x_ + v.x_, this->y_ + v.y_);
}

Vector Vector::operator-(Vector v)
{
  return Vector(this->x_ - v.x_, this->y_ - v.y_);
}

Vector Vector::operator/(double d)
{
  if (d == 0)
  {  // do not divide by zero
    ROS_ERROR("Vector has been divided by zero.");
    return Vector(0, 0);
  }

  return Vector(this->x_ / d, this->y_ / d);
}

Vector Vector::operator*(double d)
{
  return Vector(this->x_ * d, this->y_ * d);
}


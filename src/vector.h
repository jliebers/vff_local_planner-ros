#ifndef VFF_LOCAL_PLANNER_VECTOR_H
#define VFF_LOCAL_PLANNER_VECTOR_H

#include <cmath>
#include "ros/ros.h"

namespace VFF_local_planner
{

class Vector
{
private:
  double x_, y_;  // 2D Euclidean Vector

public:
  Vector();
  Vector(double x, double y);
  void setX(double x);
  void setY(double y);
  double getX();
  double getY();
  double getLen();
  Vector getUnitVector();

  Vector& operator+= (Vector v);
  Vector& operator-= (Vector v);
  Vector  operator+  (Vector v);
  Vector  operator-  (Vector v);

  Vector  operator/  (double d);
  Vector  operator*  (double d);
  Vector& operator/= (double d);
  Vector& operator*= (double d);
};

}

#endif
